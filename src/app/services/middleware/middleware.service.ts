import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MiddlewareService {
  private REST_API: string = environment.API_URL;

  constructor(private http: HttpClient) { }

  public post(endpoint: string, request: object) {
    return this.http.post(this.REST_API + endpoint, request);
  }

  public get(endpoint: string) {
    return this.http.get(this.REST_API + endpoint);
  }
}
