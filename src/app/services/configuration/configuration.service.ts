import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  constructor() { }
  public endpoints = {
    users: 'assets/json/users.json',
    roles: 'assets/json/roles.json',
  };
}
