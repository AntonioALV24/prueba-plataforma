import { Injectable } from '@angular/core';
import { Rol, User } from '../models/user.model';
import { HttpClient } from "@angular/common/http";
import { MiddlewareService } from './middleware/middleware.service';
import { ConfigurationService } from './configuration/configuration.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private users: User[];
  private roles: Rol[];

  constructor(private httpClient: HttpClient, 
    private middleware: MiddlewareService,
    private config: ConfigurationService) { 
  }

  getUsers() {
    return new Promise((resolve) => {
      this.middleware.get(this.config.endpoints.users).subscribe(
        (res: any) => {
          this.users = res.users
          return resolve(this.users);
        },
        (err: any) => {
          console.error(err);
        }
      );
    });
  }

  getRoles() {
    return new Promise((resolve) => {
      this.middleware.get(this.config.endpoints.roles).subscribe(
        (res: any) => {
          this.roles = res.roles
          return resolve(this.roles);
        },
        (err: any) => {
          console.error(err);
        }
      );
    });
  }
}
