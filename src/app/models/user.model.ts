export interface User {
    picture: string;
    name: string;
    fathersLastName: string;
    mothersLastName: string;
    email: string;
    roleId: number | string;
    active: boolean;
  }
  
  export interface Rol {
    id: number;
    position: string;
  }