import { Component, OnInit } from '@angular/core';
import { element } from 'protractor';
import { Rol, User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  columns = [
    "Foto",
    "Nombre",
    "Apellido Paterno",
    "Apellido Materno",
    "Correo",
    "Rol",
    "Status"
  ]
  users: User[] = [];
  roles: Rol[] = [];

  constructor(
    private userService: UserService,
  ) {
    this.initData();
  }

  ngOnInit(): void { 
  }

  getRoles() {
    this.userService.getRoles().then((res: Rol[]) => {
      this.roles = res;
    });
  }

  getUsers() {
    this.userService.getUsers().then((res: User[]) => {
      this.users = res;
      this.users.forEach((usr: User) => {
        usr.roleId = this.roles.find(r => r.id === usr.roleId).position;
      });
    });
  }

  initData() {
    this.getRoles();
    this.getUsers();
  }

}
